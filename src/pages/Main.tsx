import React, { useEffect, useRef, useState } from 'react';
import Form from '../components/form';
import List from '../components/list';
import './main.css';

import Axios from 'axios';
type ListProps = {
  id: number;
  title: string;
  description: string;
  completed: boolean;
};
const Main = () => {
  const [lists, setLists] = useState([] as ListProps[]);
  const baseUrl = 'http://localhost:5000';

  // const getLists = async () => {
  //   try {
  //     const response = await fetch(`${baseUrl}/lists`);
  //     const data = await response.json();
  //     setLists(data);
  //   } catch (e) {
  //     alert(e);
  //   }
  // };
  const getLists = async () => {
    try {
      const response = await Axios.get(`${baseUrl}/lists`);
      setLists(response.data);
    } catch (e) {
      alert(e);
    }
  };

  const deleteList = async (id: number) => {
    try {
      await Axios.delete(`${baseUrl}/lists/${id}`);
      getLists();
    } catch (e) {
      alert(e);
    }
  }

  const fetchData = useRef(true);
  useEffect(() => {
    if (fetchData.current) {
      getLists();
      fetchData.current = false;
    }
  }, []);

  const onComplete = (data: ListProps) => {
    console.log('onComplete data', data);
  };
  const onDelete = (id: number) => {
    deleteList(id)
  };

  const addList =async (payload: object) => {
    try {
      await Axios.post(`${baseUrl}/lists`, payload)
      getLists();
    } catch (e) {
      alert(e);
    }
  }

  const onSubmit = (event: any, data: {title: string, description: string}) => {
    event.preventDefault();
    if(data.title !== '' || data.description !== '') {
      const payload = {
        title: data.title,
        description: data.description,
        completed: false
      }
      addList(payload)
    }
  }
  return (
    <div className="main">
      <h1>My Todos</h1>
      <Form onSubmit={(event: any, data: {title: string, description: string}) => onSubmit(event, data)} />
      {lists.length > 0 ? (
        <div>
        {lists.sort((a,b) => b.id-a.id).map((list: ListProps) => (
          <List
            list={list}
            key={list.id}
            onComplete={() => onComplete(list)}
            onDelete={() => onDelete(list.id)}
          />
        ))}
      </div>
      ) : (
        <div>Nothing todo</div>
      )}
      
    </div>
  );
};

export default Main;
